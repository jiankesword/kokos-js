import kkConfig from 'kkConfig'
import kkTime from 'kkTime'
import kkApi from 'kkApi'
import kkEvent from 'kkEvent'
import kkConstant from 'kkConstant'
import kkCache from 'kkCacheData'

const getDealyTime = () => {
    let time = 0
    time += kkConfig.healthAdvice.isShow ? kkConfig.healthAdvice.showTimeS * 1000 : 0
    time += kkConfig.loading.isDealy ? kkConfig.loading.dealyTimeS * 1000 : 0
    return time
}

const dailyInfo = {
    date: kkTime.getNowDate(), // 当前日期
    isSignIn: false, // 今天是否签到

}

const baseDate = {
    dailyInfo: { // 每日数据
        date: '', // 日期
    },

    isNewPlayer: true, // 是否是新玩家
    isOpenSound: true, // 是否开启声音

    soundSetting: { // 声音设置
        isOpenBgm: true, // 背景音乐是否开启
        isOpenVoice: true, // 音效是否开启
    },

    statisticalData: { // 统计数据
        totalLoginData: 0, // 总登录过的天数
        totalOnlineMinute: 0, // 在线总时长
    },

    signInData: new Array(7).fill(false),

    gold: 0, // 金币
    power: kkConfig.power.originalValue, // 体力
    powerCycles: kkConfig.power.recoverCycleS, // 距离下次加体力的秒数
    // ------------------- method -----------------------

    loadPlayerData: () => { // 加载玩家数据
        return new Promise((resolve, reject) => {
            const dealyTime = getDealyTime()
            setTimeout(() => {
                kkApi.initGameData().then(() => {
                    kkTime.initTimer() // 数据加载完成的时候，初始化时间模块
                    baseDate.openPowerCounter() // 开启体力计数器
                    resolve()
                })
            }, dealyTime)
        })

    },

    resetDailyInfo() { // 重置每日数据
        this.dailyInfo = JSON.parse(JSON.stringify(dailyInfo))
    },

    getGold() {
        // cc.log(this)
        return this.gold
    },

    setGold(value) {
        if (typeof value !== 'number') {
            throw new Error('金币数值 类型错误')
        }
        if (value < 0) {
            throw new Error('金币数值不能小于 0')
        }
        this.gold = value
        kkApi.saveData()
        kkEvent.emit(kkConstant.EVENT.refreshGold)
    },

    getPower() {
        return this.power
    },

    setPower(value) {
        if (value < 0) {
            throw new Error('体力数值不能小于 0')
        }
        const { upperLimit, recoverCycleS } = kkConfig.power

        if (this.power >= upperLimit && value < upperLimit) {
            this.powerCycles = recoverCycleS
        }

        this.power = value

        kkApi.saveData()
        kkEvent.emit(kkConstant.EVENT.refreshPower)
    },

    openPowerCounter() { // 打开体力计数器
        const { isAble, upperLimit, recoverCycleS, recoverAmount } = kkConfig.power
        const { offlineSeconds } = kkCache

        if (!isAble) {
            return
        }

        const offlinePower = Math.floor(offlineSeconds / recoverCycleS) // 离线时增长的体力
        const surplusCycles = offlineSeconds % recoverCycleS // 剩余的体力增长时间

        this.power + offlinePower >= upperLimit ?
            (this.power >= upperLimit ? null : this.power = upperLimit) :
            this.power += offlinePower // 离线增长的体力值

        this.powerCycles = surplusCycles
        kkApi.saveData()

        isNaN(this.power) ? this.power = kkConfig.power.originalValue : null
        isNaN(this.powerCycles) ? this.powerCycles = recoverCycleS : null

        setInterval(() => {
            if (this.power < upperLimit) {
                if (this.powerCycles > 0) {
                    this.powerCycles--
                } else {
                    this.powerCycles = recoverCycleS
                    this.power += recoverAmount
                    kkApi.saveData()
                }
            }
            kkEvent.emit(kkConstant.EVENT.refreshPower)
        }, 1 * 1000)
    },

    signIn() {
        const s = this.signInData
        const isFull = s.indexOf(false) == -1

        if (this.dailyInfo.isSignIn == true) {
            console.warn('今天已经签到过了')
        }

        if (isFull) {
            this.signInData = new Array(7).fill(false)
            this.signInData[0] = true
        } else {
            for (let i = 0; i < s.length; i++) {
                if (s[i] == false) {
                    s[i] = true
                    break
                }
            }
        }

        this.dailyInfo.isSignIn = true

        kkApi.saveData()
    },
}

export default baseDate