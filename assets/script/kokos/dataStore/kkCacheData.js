// 缓存数据，只作为单次游戏的缓存，不保存到数据库的数据
const cacheData = {
    playerToken: { // 各种渠道登录的 id
        wxOpenId: ''
    },
    isDataInit: false, // 是否已经初始化过数据
    offlineSeconds: 0, // 最近一次离线时间 / 秒
    isFirstLoading: true, // 是否首次（每一次打开游戏的首次）加载游戏（判断是否加载健康忠告）
    nextScene: '', // 下一个场景，用于 loading 后转跳
}

export default cacheData