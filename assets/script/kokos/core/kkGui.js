import actionHelper from 'kkAction'

// 弹窗管理
const windowsWrapper = {
    prefabMap: {},

    /**
     * 弹窗 promise.then 会返回这个弹窗的节点
     * @param {string} prefabPath 相对 resources 目录，弹窗预制体的路径
     * @param {cc.Node} parentNode 弹窗父节点，缺省为当前场景 Canvas 根节点
     */
    pop(prefabPath, parentNode) {
        let prefab = this.prefabMap[prefabPath]
        parentNode = parentNode || cc.director.getScene().getChildByName("Canvas")

        return new Promise((resolve, reject) => {
            if (prefab) {
                let popNode = cc.instantiate(prefab)
                parentNode.addChild(popNode, 99)
                resolve(popNode)

            } else { // 如果没缓存过这个界面的话就缓存起来，下次可以直接加载
                this.showLoading(true)
                cc.loader.loadRes(prefabPath, (err, prefab) => {
                    this.showLoading(false)
                    if (err) {
                        cc.log("async load prefab failed : ", prefabPath)
                        reject()
                    }
                    this.prefabMap[prefabPath] = prefab
                    let popNode = cc.instantiate(prefab)
                    parentNode.addChild(popNode, 99)
                    resolve(popNode)

                })
            }
        })
    },

    /**
     * 活跃的弹窗 这个效果和 pop 相同，但是这个 api 弹出的窗口会附带开关动画
     * @param {string} prefabPath 相对 resources 目录，弹窗预制体的路径
     * @param {cc.Node} parentNode 弹窗父节点，缺省为当前场景 Canvas 根节点
     */
    vibrantPop(prefabPath, parentNode) {
        let prefab = this.prefabMap[prefabPath]
        parentNode = parentNode || cc.director.getScene().getChildByName("Canvas")

        return new Promise((resolve, reject) => {
            if (prefab) {
                let popNode = cc.instantiate(prefab)
                parentNode.addChild(popNode, 99)
                popNode.runAction(actionHelper.windowOpenAction())

                popNode.originDestroy = popNode.destroy
                popNode.destroy = () => {
                    popNode.runAction(actionHelper.windowCloseAction())
                    setTimeout(() => {
                        popNode.originDestroy()
                    }, 500)
                }

                resolve(popNode)

            } else { // 如果没缓存过这个界面的话就缓存起来，下次可以直接加载
                this.showLoading(true)
                cc.loader.loadRes(prefabPath, (err, prefab) => {
                    this.showLoading(false)
                    if (err) {
                        cc.log("async load prefab failed : ", prefabPath)
                        reject()
                    }
                    this.prefabMap[prefabPath] = prefab
                    let popNode = cc.instantiate(prefab)
                    parentNode.addChild(popNode, 99)
                    popNode.runAction(actionHelper.windowOpenAction())

                    popNode.originDestroy = popNode.destroy
                    popNode.destroy = () => {
                        popNode.runAction(actionHelper.windowCloseAction())
                        setTimeout(() => {
                            popNode.originDestroy()
                        }, 500)
                    }

                    resolve(popNode)

                })
            }
        })
    },

    /**
     * 通用提示窗, override
     * @param {string} message 提示信息
     */
    showMessage(message) {
        this.pop('popWindows/messagePop')
            .then((messagePop) => {
                messagePop.getComponent("messagePop").initMessage(message)
            })
    },

    /**
     * 弹出剧情弹窗
     * @param config 剧情配置
     */
    showPlot(config) {
        // this.pop('perfab/ui/plotPop')
        //     .then((plotPop) => {
        //         plotPop.getComponent('plotPop').initPlot(config)
        //     })
    },

    /**
     * loading 转圈等待展示(屏蔽点击)
     * @param {Boolean} isShow 显示或隐藏 loading 界面
     */
    showLoading(isShow) {
        let loadingMask = this.prefabMap['loadingMask']
        if (loadingMask) {
            loadingMask.active = isShow
        } else if (!loadingMask && this.prefabMap.isLoadedLoading) { // 有可能在异步加载中，不这样做的话可能会生成多个
            let timer = setInterval(() => {
                if (this.prefabMap['loadingMask']) {
                    this.prefabMap['loadingMask'].active = false
                    clearInterval(timer)
                }
            }, 300)
        } else {
            this.prefabMap.isLoadedLoading = true
            cc.loader.loadRes('popWindows/loadingMask', (err, prefab) => {
                if (err) {
                    cc.log("kkGui Error showLoading")
                }
                const scene = cc.director.getScene()
                loadingMask = cc.instantiate(prefab)
                scene.addChild(loadingMask, 999)

                this.prefabMap['loadingMask'] = loadingMask

                loadingMask.active = isShow
            })
        }
    },

}

export default windowsWrapper
