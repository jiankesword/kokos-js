import kkEvent from 'kkEvent'
import kkSound from 'kkSound'
import kkGui from 'kkGui'

import kkApi from 'kkApi'
import kkCache from 'kkCacheData'
import kkBaseData from 'kkBaseData'

import kkAction from 'kkAction'
import kkAdapt from 'kkAdapt'
import kkNumber from 'kkNumber'
import kkTime from 'kkTime'
import kkConstant from 'kkConstant'
import kkFile from 'kkFile'

import kkLottery from 'kkLottery'

import kkConfig from 'kkConfig'

import { parallelLoadPackage, serialLoadPackage } from '../utils/subpackageLoader'

const kk = {
    event: kkEvent,
    sound: kkSound,
    gui: kkGui,

    api: kkApi,
    cache: kkCache,
    baseData: kkBaseData,

    action: kkAction,
    adapt: kkAdapt,
    num: kkNumber,
    time: kkTime,
    constant: kkConstant,
    file: kkFile,

    lottery: kkLottery,

    config: kkConfig,

    parallelLoadPackage,
    serialLoadPackage
}

// window.kk = kk

export default kk