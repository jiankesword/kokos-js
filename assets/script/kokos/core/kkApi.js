import kkConfig from 'kkConfig'
import kkBaseData from 'kkBaseData'
import { getNowDate } from 'kkTime'

/**
 * 保存数据 
 * @return Promise
 */
const saveData = () => new Promise((resolve, reject) => {
    if (kkConfig.isNetWork === false) { // 无网络状态下储存数据到缓存
        const dataString = JSON.stringify(kkBaseData)
        cc.sys.localStorage.setItem(kkConfig.baseDataKey, dataString)
        // cc.log('无网络环境，数据储存到本地：', dataString)
        resolve()
    } else {
        // if (cc.sys.platform === cc.sys.WECHAT_GAME) { // 微信环境下
        //     // cloudDBWrapper.updataCommonData(issueConfig.cacheKey, playerData)
        // }
    }
})

/**
 * 加载玩家历史数据
 */
const initGameData = () => new Promise((resolve, reject) => {
    if (kkConfig.isNetWork === false) { // 无网络状态下储存数据到缓存
        let dataString = cc.sys.localStorage.getItem(kkConfig.baseDataKey)
        if (dataString) {
            // cc.log("无网络环境 加载玩家浏览器缓存的数据:", dataString)
            updatePlayerData(JSON.parse(dataString))
        } else {
            // cc.log('无网络环境 新玩家 新建原始数据存档', dataString)
            saveData()
            updateDayInfo()
        }

        resolve()
    } else {
        // if (cc.sys.platform === cc.sys.WECHAT_GAME) { // 微信环境下
        //     // cloudDBWrapper.updataCommonData(issueConfig.cacheKey, playerData)
        // }
    }

})

// 更新玩家数据 变成加载后的
const updatePlayerData = (loadedDataObj) => {
    let keysArray = Object.keys(loadedDataObj)
    for (let i = 0; i < keysArray.length; i++) {
        let key = keysArray[i]
        kkBaseData[key] = loadedDataObj[key]
    }
    updateDayInfo()
    // cc.log(Object.keys(loadedDataObj))
}

// 初始化每日数据 检查上一次更新每日数据的时间和今天是不是同一天，不是的话就重置每日数据
const updateDayInfo = () => {
    let lastDate = kkBaseData.dailyInfo.date // 上次更新的时间
    let nowDate = getNowDate()
    if (lastDate != nowDate) {
        kkBaseData.resetDailyInfo()
    }
    saveData()
}


const kkApi = {
    saveData,
    initGameData
}

export default kkApi