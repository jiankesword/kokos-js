import kkNumber from "kkNumber"

// 泡泡向上飞行消失的动作
const bubbleAction = () => {
    const time = kkNumber.getRandomFloatNum(0.5, 1.5)
    const distance = kkNumber.getRandomFloatNum(100, 300)
    const flyUp = cc.moveBy(time, 0, distance)
    const fadeOut = cc.fadeOut(time)
    const spa = cc.spawn(flyUp, fadeOut)
    const removeSelfAction = cc.removeSelf()

    return cc.sequence(spa, removeSelfAction)
}

// 弹窗的窗口动作
const windowOpenAction = () => {
    const time1 = 0.2
    const time2 = 0.1
    const scale1_2 = cc.scaleTo(time1, 1.2)
    const scale1_1 = cc.scaleTo(time2, 1.1)
    const scale1_0 = cc.scaleTo(time2, 1)

    return cc.sequence([scale1_2, scale1_0, scale1_1, scale1_0])
}

// 弹窗关闭动作
const windowCloseAction = () => {
    const time1 = 0.2
    const time2 = 0.1
    const scale1_2 = cc.scaleTo(time1, 1.2)
    const scale1_1 = cc.scaleTo(time2, 1.1)
    const scale1_0 = cc.scaleTo(time2, 1)

    return cc.sequence([scale1_2, scale1_0, scale1_1, scale1_0])
}

// icon 飞起落下消失
const iconJumpAction = () => {
    const time1 = 0.3
    const time2 = 0.8
    const moveUp = cc.moveBy(time1, cc.v2(-10, 100)).easing(cc.easeOut(2))
    const moveDown = cc.moveBy(time2, cc.v2(200, -1200)).easing(cc.easeIn(2))

    const moveSeq = cc.sequence(moveUp, moveDown)
    const fadeAction = cc.fadeOut(time1 + time2)

    return cc.spawn(moveSeq, fadeAction)
}

const actionHelper = {
    bubbleAction,
    windowOpenAction,
    windowCloseAction,
    iconJumpAction
}

export default actionHelper
