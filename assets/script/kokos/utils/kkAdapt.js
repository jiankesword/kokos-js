import kkConfig from 'kkConfig'

const adaptCanvas = () => {
    const canvas = cc.find('Canvas').getComponent(cc.Canvas)
    const designRatio = kkConfig.designResolution.width / kkConfig.designResolution.height
    const screenRatio = cc.winSize.width / cc.winSize.height

    if (screenRatio > designRatio) {
        canvas.fitWidth = false
        canvas.fitHeight = true
    } else {
        canvas.fitWidth = false
        canvas.fitHeight = true
    }

    // if (screenRatio <= 0.67) {
    //     canvas.fitWidth = true
    //     canvas.fitHeight = false
    // } else if (ratio > 1) {
    //     canvas.fitWidth = true
    //     canvas.fitHeight = true
    // } else {
    //     canvas.fitWidth = true
    //     canvas.fitHeight = false
    // }
}

export default adaptCanvas