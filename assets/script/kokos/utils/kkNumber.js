/**
 * 随机生成 [min, max) 范围内的整数
 * @param {Number} min 最小值，包括
 * @param {Number} max 最大值，不包括
 */
const getRandomIntNum = (min, max) => {
    return Math.floor(Math.random() * (max - min)) + min
}

/**
 * 随机生成 [min, max) 范围内的浮点数
 * @param {Number} min 最小值，包括
 * @param {Number} max 最大值，包括
 */
const getRandomFloatNum = (min, max) => {
    return Math.random() * (max - min) + min
}

/**
 * 角度标准化， 把角度转换成0到360度
 * @param {Number} angle 角度 
 */
const angleToStandard = (angle) => {
    let standerAngle = 0
    if (angle < 0) {
        standerAngle = 360 - Math.abs(angle % 360)
    } else {
        standerAngle = angle % 360
    }
    return standerAngle
}

const numberHelper = {
    getRandomIntNum,
    getRandomFloatNum,
    angleToStandard
}

export default numberHelper
