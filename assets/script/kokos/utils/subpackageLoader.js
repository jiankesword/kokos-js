export const loadSubpackage = (packageName) => new Promise((resolve, reject) => {
    cc.loader.downloader.loadSubpackage(packageName, (err) => {
        if (err) {
            reject(err)
        }
        // console.log(`load subpackage ${packageName} successfully`)
        resolve()
    })
})

// 并行加载 
export const parallelLoadPackage = (packageNameList) => new Promise((resolve, reject) => {
    if (packageNameList.length == 0) {
        reject('子包名列表为空')
    }

    const promiseList = []
    packageNameList.forEach(name => {
        const loadPromise = loadSubpackage(name)
        promiseList.push(loadPromise)
    })


    Promise.all(promiseList)
        .then(() => resolve())
})

// 串行加载
export const serialLoadPackage = (packageNameList) => new Promise((resolve, reject) => {
    if (packageNameList.length == 0) {
        reject('子包名列表为空')
    }
    let currentPromise = Promise.resolve()
    packageNameList.forEach(name => {
        currentPromise = currentPromise.then(() => loadSubpackage(name))
    })

    currentPromise
        .then(() => resolve())
})