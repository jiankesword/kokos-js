// 常量定义在这个脚本

const constantCollection = {
    GOODSTYPE: { // 商品类型

    },

    EVENT: { // 事件枚举
        refreshGold: 'refreshGold', // 刷新金币值的 ui
        refreshPower: 'refreshPower', // 刷新体力值的 ui
    }
}

export default constantCollection