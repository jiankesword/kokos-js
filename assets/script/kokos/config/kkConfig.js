const kkConfig = {
    isNetWork: false, // 是否有网络，有网络时会发起短连接，无网络时数据则保存在缓存中
    isDebug: true, // 是否调试模式，显示调试工具
    healthAdvice: { // 健康忠告
        isShow: true, // 是否显示
        showTimeS: 2, // 显示时间 / 秒
    },
    loading: { // loading 设置
        isDealy: true, // 是否延时 loading
        dealyTimeS: 1, // 延时的时间 / 秒
    },
    power: { // 体力
        isAble: true, // 是否开启体力值
        originalValue: 3, // 初始值
        upperLimit: 20, // 体力值上限  
        canBreakLimit: true, // 体力是否能突破上限
        recoverCycleS: 1 * 60, // 体力恢复周期 / 秒
        recoverAmount: 1, // 每个周期恢复的数值
    },
    firstScene: 'exampleMenu', // loading 完成后要加载的场景名称
    baseDataKey: '__kk_example$%*', // 储存数据的 key，任意的字符串，建议写复杂一些不容易与其他 key 冲突
    sound: {
        bgmUrl: 'sounds/background_music', // 背景音乐的 url
        bgmVolume: 0.6, // 背景音乐音量
        effectVolume: 1, // 音效的音量
    },
    designResolution: { // 设计分辨率，适配用
        height: 1334,
        width: 750
    }
}

export default kkConfig
