cc.Class({
    extends: cc.Component,

    start() {
        const upAction = cc.moveBy(1, 0, 300)
        const hidddenAction = cc.fadeTo(1, 0)
        const spawnAction = cc.spawn(upAction, hidddenAction)
        this.scheduleOnce(() => {
            this.node.runAction(spawnAction)
            this.scheduleOnce(() => {
                this.node.destroy()
            }, 0.8)
        }, 1)
    },

    initMessage(message) {
        let label = this.node.getChildByName('label').getComponent(cc.Label)
        label.string = message
    }

});
