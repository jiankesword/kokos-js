cc.Class({
    extends: cc.Component,

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        const loadingImgNode = this.node.getChildByName('loading')
        const rotateAction = cc.rotateBy(1.5, 360)
        const repeatAction = cc.repeatForever(rotateAction)

        loadingImgNode.runAction(repeatAction)

        cc.game.addPersistRootNode(this.node)

        this.node.x = cc.winSize.width / 2
        this.node.y = cc.winSize.height / 2
    },

    start() {

    },

    // update (dt) {},
});
