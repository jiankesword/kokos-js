import kk from 'kokos'

cc.Class({
    extends: cc.Component,

    onLoad() {
        this.descLabel = this.node.getChildByName('uiBottom').getChildByName('desc').getComponent(cc.Label)
        this.offlineMinute = Math.floor(kk.cache.offlineSeconds / 60)
        this.awardGold = Math.ceil(0.2 * this.offlineMinute)

        this.initDesc()

    },

    initDesc() {
        // cc.log('离线时间：', kk.cache.offlineSeconds)
        this.descLabel.string = `欢迎回来，本次离线时长
        ${this.offlineMinute} 分钟
        奖励 ${this.awardGold} 金币`

    },

    handleConfirm() {
        const bd = kk.baseData
        bd.setGold(bd.getGold() + this.awardGold)
        this.handleClose()
    },

    handleClose() {
        this.node.destroy()
    }

    // update (dt) {},
});
