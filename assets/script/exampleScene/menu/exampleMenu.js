import kk from 'kokos'

cc.Class({
    extends: cc.Component,

    onLoad() {
        kk.adapt()

        if (kk.cache.isFirstLoading) {
            kk.gui.vibrantPop('popWindows/offlineWindow')
            kk.cache.isFirstLoading = false
        }

    },

    start() {

    },

    handleLoadGameScene() {
        kk.cache.nextScene = 'exampleGame'
        cc.director.loadScene('loading')
    },

    handleLottery() {
        kk.gui.vibrantPop('popWindows/lotteryWindow')
    },

    // update (dt) {},
});
