import kk from 'kokos'
import message from '../../gameConfig/messageConfig'

cc.Class({
    extends: cc.Component,

    onLoad() {
        const isShow = kk.config.isDebug
        const canvas = cc.director.getScene().getChildByName('Canvas')
        this.debugTool = canvas.getChildByName('debugTool')
        this.debugBox = this.debugTool.getChildByName('debugBox')

        if (isShow) {
            this.debugTool.active = true
        } else {
            this.debugTool.destroy()
        }

    },

    start() {

    },

    openDebugBox() {
        this.debugBox.active = true
    },

    closeDebugBox() {
        this.debugBox.active = false
    },

    addGold() {
        const bd = kk.baseData
        bd.setGold(bd.getGold() + 10)
    },

    subGold() {
        const bd = kk.baseData
        bd.setGold(bd.getGold() - 10)
    },

    popMessage() {
        kk.gui.showMessage(message.chirp)
    },

    addPower() {
        const bd = kk.baseData
        bd.setPower(bd.getPower() + 1)
    },

    subPower() {
        const bd = kk.baseData
        bd.setPower(bd.getPower() - 1)
    },

    showLoading() {
        kk.gui.showLoading(true)
        this.scheduleOnce(() => {
            kk.gui.showLoading(false)
        }, 2)
    },

    upLoadConfig() {
        kk.file.uploadJsonFile()
            .then((dataString) => {
                const data = JSON.parse(dataString)
                console.log('测试配置上传成功', data)
            })
    }

    // update (dt) {},
});
