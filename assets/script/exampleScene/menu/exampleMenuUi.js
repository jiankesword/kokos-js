import kk from 'kokos'

cc.Class({
    extends: cc.Component,

    onLoad() {
        const scene = cc.director.getScene()
        const canvas = scene.getChildByName('Canvas')

        this.viewLayer = canvas.getChildByName('uiLayer').getChildByName('viewLayer')
        this.powerView = this.viewLayer.getChildByName('powerView')

        this.initEventListener()
        this.refreshPowerView()
    },

    start() {

    },

    initEventListener() {
        const eventManager = kk.event
        const eventEnum = kk.constant.EVENT
        eventManager.on(eventEnum.refreshPower, this.refreshPowerView, this)

    },

    removeEventListener() {
        const eventManager = kk.event
        const eventEnum = kk.constant.EVENT
        eventManager.off(eventEnum.refreshPower, this.refreshPowerView, this)

    },

    refreshPowerView() {
        const powerLabel = this.powerView.getChildByName('label').getComponent(cc.Label)
        const timeNode = this.powerView.getChildByName('updateTime')
        const { power, powerCycles } = kk.baseData
        const { upperLimit } = kk.config.power
        const powerIsFull = power >= upperLimit

        powerLabel.string = `${power} / ${upperLimit}`

        timeNode.active = !powerIsFull
        !powerIsFull ? timeNode.getComponent(cc.Label).string = kk.time.formatSeconds(powerCycles) : null
    },

    onDestroy() {
        this.removeEventListener()
    }

    // update (dt) {},
});
