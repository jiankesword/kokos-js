import kk from 'kokos'

cc.Class({
    extends: cc.Component,

    onLoad () {
        kk.adapt()

    },

    start() {

    },

    handleLoadMenuScene() {
        kk.cache.nextScene = 'exampleMenu'
        cc.director.loadScene('loading')
    }

    // update (dt) {},
});
