export default [
    {
        id: 1, // id
        type: 'gold', // 奖励类型
        img: 'img/substance/gold', // 图片路径
        value: 10, // 奖励数值
        probability: 9.99, // 中奖概率，百分比 percent
    },
    {
        id: 2, // id
        type: 'gold', // 奖励类型
        img: 'img/substance/gold', // 图片路径
        value: 30, // 奖励数值
        probability: 10.01, // 中奖概率，百分比 percent
    },
    {
        id: 3, // id
        type: 'gold', // 奖励类型
        img: 'img/substance/gold', // 图片路径
        value: 80, // 奖励数值
        probability: 10, // 中奖概率，百分比 percent
    },
    {
        id: 4, // id
        type: 'gold', // 奖励类型
        img: 'img/substance/gold', // 图片路径
        value: 60, // 奖励数值
        probability: 20, // 中奖概率，百分比 percent
    },
    {
        id: 5, // id
        type: 'gold', // 奖励类型
        img: 'img/substance/gold', // 图片路径
        value: 20, // 奖励数值
        probability: 10, // 中奖概率，百分比 percent
    },
    {
        id: 6, // id
        type: 'gold', // 奖励类型
        img: 'img/substance/gold', // 图片路径
        value: 30, // 奖励数值
        probability: 10, // 中奖概率，百分比 percent
    },
    {
        id: 7, // id
        type: 'gold', // 奖励类型
        img: 'img/substance/gold', // 图片路径
        value: 90, // 奖励数值
        probability: 20, // 中奖概率，百分比 percent
    },
    {
        id: 8, // id
        type: 'gold', // 奖励类型
        img: 'img/substance/gold', // 图片路径
        value: 66, // 奖励数值
        probability: 10, // 中奖概率，百分比 percent
    },
]