const messageConfig = {
    chirp: '嘤嘤嘤~',
    notEnoughGold: '金币不足',

    alreadySignIn: '今天已经签到',
    shareSuccess: '分享成功',
    shareFail: '分享失败'
}

export default messageConfig