// 在组件原型上添加一个事件队列的方法
export const addScheduleQueueToComponent = () => {
    cc.Component.prototype.excuteScheduleQueue = function (eventList, timeList) {
        let time = 0
        for (let i = 0; i < eventList.length; i++) {
            if (typeof timeList[i] != 'number') {
                throw new Error('时间列表参数错误：excuteScheduleQueue')
            }
            time += timeList[i]
            this.scheduleOnce(() => {
                if (typeof eventList[i] == 'function') {
                    eventList[i]()
                } else {
                    console.warn('事件队列，请传入函数：excuteScheduleQueue')
                }
            }, time)
        }
    }
}

// 在节点原型上添加渐显渐隐方法
export const addSlowlyChangeToNode = () => {
    /**
     * 节点渐显
     */
    cc.Node.prototype.showSlowly = function () {
        this.opacity = 0
        this.active = true
        this.runAction(cc.fadeIn(0.5))
    }

    /**
     * 节点渐隐
     */
    cc.Node.prototype.hideSlowly = function () {
        this.runAction(cc.fadeOut(0.5))
        this.active = false
        this.opacity = 255
    }
}