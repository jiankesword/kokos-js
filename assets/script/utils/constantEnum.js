export const eventType = { // 事件类型

    // -------- ui --------

    showGameUi: 'show_game_ui', // 显示游戏 ui
    hideGameUi: 'hide_game_ui', // 隐藏游戏 ui

    stopScroll: 'stop_scroll', // 停止 scroll 滑动
    recoverScroll: 'recover_scroll', // 恢复 scroll 滑动

    gameWin: 'game_win', // 游戏胜利
    gameDefeat: 'game_defeat', // 游戏失败

    hideTouchEffect: 'hide_touch_effect', // 隐藏点击特效
    hidePropDesc: 'hide_prop_desc', // 隐藏道具说明

    //  -------- game --------------

    startCountDown: 'start_count_down', // 开始计时
    stopCountDown: 'stop_count_down', // 停止计时
    timeOut: 'time_out', // 计时完成（超时

    openTouchMask: 'open_touch_mask', // 打开触碰遮挡（不能触碰了
    closeTouchMask: 'close_touch_mask', // 关闭触碰遮挡（能继续点击

    replayGame: 'replay_game', // 重新开始游戏

    showGuide: 'show_guide', // 展示新手引导
    hideGuide: 'hide_guide', // 隐藏新手引导 

    refreshGold: 'refresh_gold', // 刷新金币
    refreshPower: 'refresh_power', // 刷新体力
    refreshLevelNum: 'refresh_level_num', // 刷新关卡显示

    showChangeScene: 'show_change_scene', // 转场遮罩
}