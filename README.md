# kokos

* 一个简单的 cocos creator 框架和一些规范，适合小型游戏快速开发
* 让开发者能够把更多精力放在游戏玩法上
* 建议使用 cocos creator 版本 2.2.1+
* 已经被多个线上的项目使用，会持续更新

## 如何使用

* 将本项目中的 /assets/resources/script/kokos 目录，复制粘贴到要使用本项目的工程的脚本目录下
* 可以将其他实例资源也复制到工程目录下
* 如果是从零开始开发一个项目，建议直接在本项目中开始

## 理念

* 让更多的东西配置化，降低维护负担

## api 

### 适配

* 需要先在配置文件中配置好设计分辨率
* 在进入每个游戏场景时调用 ``` kk.adapt() ```

### 事件

* kk.event 对事件的处理
* 建议在数据层发布事件，在 ui 层订阅事件  
* 原本是使用 ES6 的 EventTarget，目前改成对事件处理更加完善的 ` `  ` new cc. Node() `  ` ` 

``` javascript
// 以加金币后刷新 ui 显示为例子，其中 gold 是金币的数值

// ---------- 管理 ui 的脚本 -----------------------------------
import kk from 'kokos'

// 首先在控制 ui 的脚本监听刷新金币的事件
kk.event.on('refreshGold', refreshUi, this)

// 刷新 ui 的函数
function refreshUi() {
    ui.label.string = gold
}

// --------- 管理数据的脚本 -------------------------------------
import kk from 'kokos'

// 加金币后派发刷新事件，就能刷新 ui
function addGold(value) {
    gold += value
    kk.event.emit('refreshGold')
}
```

## 声音管理

## 分包加载

* 分包加载可以选择串行加载或者并行加载
* ` `  ` kk.parallelLoadPackage `  ` ` 
* ` `  ` kk.serialLoadPackage `  ` ` 
* 具体用法可以参考这个仓库 https://gitee.com/jiankesword/cc-subpackage-loader

### 切换场景

* 使用 loading 场景可以优化游戏体验，加载远程资源或者较大的场景时不至于黑屏或者卡顿
* loading 场景及其所有资源都在 /assets/loading 目录下，作为分包，能够确保首屏快速加载
* 需要在两个场景切换之间加载 loading 场景可以如此

``` javascript
kk.cache.nextScene = 'exampleGame' // 先将需要转跳的场景名赋值到这里
cc.director.loadScene('loading') // 再转跳 loading 场景
```

### 一些扩充的功能

* 在 methodHelper.js 中，调用 addScheduleQueueToComponent 这个函数可以给 cc.Component 增加一个事件队列功能，传入 eventList 一个函数数组和 timeList 一个数字数组。 eventList 是需要执行的事件， timeList 是每件事之间的时间间隔
* 调用 addSlowlyChangeToNode 这个函数可以给 cc.Node 增加渐隐和渐显的方法
* 考虑到这些需求不是每个项目都会有，所以要用到的话需要自己主动添加

### 常用的属性

#### 货币

* 小游戏一般都会有金币和体力两种货币，所以暂时只提供这两种经典的示例
* 在 kokos/dataStore/kkBaseData.js 中可以找到这两种货币相关的代码
* 金币 gold
    - 获取玩家当前的金币数值 kk.baseData.getGold()
    - 设置玩家当前的金币数值 kk.baseData.setGold(value)
    - 根据项目的业务逻辑进行金币操作即可，金币数值不能小于 0 否则会抛出异常

* 体力 power
    - 体力属于一种会自动调整的属性，可以在 kkConfig.js 中根据业务对体力值的需求进行配置，配置能满足大部分游戏的要求，无需关心体力值的增减问题
    - 可配置的属性包括：是否启动体力，新玩家的体力初始值，体力的恢复周期，每次恢复体力的值，体力上限，体力是否能超出上限等。有其它相关的需求可以在 Issues 中提出
    - 获取玩家当前的金币数值 kk.baseData.getPower()
    - 设置玩家当前的金币数值 kk.baseData.setPower(value)
    - 体力实时刷新可以参照示例场景的代码

#### 属性

* 离线时间(秒)
    - 有时候需要根据离线时间对玩家进行奖励，可以通过下面的属性访问玩家的离线时长
    - kk.cache.offlineSeconds

### 产品化

* 需要抽奖，签到，成就，任务等模块提高小游戏玩家留存
* 在设计上不同项目的产品化内容应该只是参数和表现不同，这里会提供一套与 ui 分离的逻辑

## 抽奖

* 使用前需要先配置抽奖，参照范例中 script/gameConfig/lotteryConfig.js

* 抽奖配置如下，概率单位为 % 所有奖品的概率加起来必须等于 1，否则不能正常使用

``` javascript
[{
    id: 1, // id
    type: 'gold', // 奖励类型
    img: 'img/substance/gold', // 图片路径
    value: 10, // 奖励数值
    probability: 9.99, // 中奖概率，百分比 percent
}, ]
```

``` javascript
    // 使用前先初始化
    kk.lottery.init(config)

    // 抽奖会按照概率返回对应的配置索引
    const awardIndex = kk.lottery.getAwardIndex()
    const award = config[awardIndex]
```

## 配置

* 在 kokos/config/kkConfig.js 下可以对项目进行配置

> 可以配置的属性

> isNetwork: Boolean 是否有网络，有网络时会发起短连接，无网络时数据则保存在缓存中。目前只有无网络状态

> healthAdvice.isShow: Boolean 是否显示健康忠告，在国内上线的游戏必须显示健康忠告。healthAdvice.showTimeS: Number 显示时间（秒）

> loading.isDealy: Boolean 是否延时 loading 场景，loading.dealyTimeS: Number 延时的时间（秒）

> firstScene: String 进入游戏 loading 完成后首次进入的场景

> baseDataKey: String 储存 kkBaseData 的 key 切勿与其他 key 相冲突

> power.isAble: Boolean 是否启用体力值，不需要的话可以关闭，减少性能开销。power.originalValue: Number 体力初始值。power.upperLimit: Number 体力上限。 power.recoverCycleS: Number 体力恢复周期 / 秒，可以理解为 n 秒后恢复一点体力。power.recoverAmount: Number 每个周期恢复的体力数值

## 其它功能
### 点击反馈
* loding 场景的 touchLayer 节点，所有点击都会触发一个点击反馈，脚本里有事件穿透和特效的代码，可以自行阅读

## 功能

## 更新日志
* 2020-11-22 增加了一个点击反馈功能，以后考虑新增一些常用套路

## 支持作者

* 鼓励和支持作者可以加速这个项目的更新
* 想支持作者可以购买 **火鸡面** 淘口令：￥SM0L1wTVT0g￥
